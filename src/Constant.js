// ## ES6 style
export const myconst =
{
  // api_host_port: 'http://localhost:3000'
  api_host_port: 'http://192.168.1.31:7777'
  // api_host_port: 'http://scrapy-web.jingcai.trade:7777'
  // api_host_port: 'http://scrapy-web.caiex.com:7777'
}

// // ## ES5 style
// const myconst =
// {
//   // api_host_port: 'http://localhost:3000'
//   // api_host_port: 'http://192.168.1.31:5000'
//   api_host_port: 'http://scrapy-web.jingcai.trade:7777',
//   apiHostPort: 'http://scrapy-web.jingcai.trade:7777'
//   // api_host_port: 'http://scrapy-web.caiex.com:7777'
// }
// module.exports = myconst
