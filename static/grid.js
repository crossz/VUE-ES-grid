// var apiHostPort = 'http://192.168.1.31:5000'
// var apiHostPort = 'http://scrapy-web.jingcai.trade:7777'
// var apiHostPort = 'http://scrapy-web.caiex.com:7777'

// register the grid component
Vue.component('demo-grid', {
  template: '#grid-template',
  replace: true,
  props: {
    data: Array,
    columns: Array,
    filterKey: String,
    filterKeyOddstype: String,
    queryRangeDays: String
  },
  data: function () {
    var sortOrders = {}
    this.columns.forEach(function (key) {
      sortOrders[key] = 1
    })
    return {
      sortKey: '',
      sortOrders: sortOrders
    }
  },
  computed: {
    filteredData: function () {
      var sortKey = this.sortKey
      var filterKey = this.filterKey && this.filterKey.toLowerCase()
      var filterKeyOddstype = this.filterKeyOddstype && this.filterKeyOddstype.toLowerCase()
      var order = this.sortOrders[sortKey] || 1
      var data = this.data
      if (filterKey) {
        data = data.filter(function (row) {
          return Object.keys(row).some(function (key) {
            return String(row[key]).toLowerCase().indexOf(filterKey) > -1
          })
        })
      }
      if (filterKeyOddstype) {
        data = data.filter(function (row) {
          return Object.keys(row).some(function (key) {
            key = 'odds_type'
            return String(row[key]).toLowerCase().indexOf(filterKeyOddstype) > -1
          })
        })
      }
      if (sortKey) {
        data = data.slice().sort(function (a, b) {
          a = a[sortKey]
          b = b[sortKey]
          return (a === b ? 0 : a > b ? 1 : -1) * order
        })
      }
      return data
    }
  },
  filters: {
    capitalize: function (str) {
      return str.charAt(0).toUpperCase() + str.slice(1)
    }
  },
  methods: {
    sortBy: function (key) {
      this.sortKey = key
      this.sortOrders[key] = this.sortOrders[key] * -1
    }
  }
})

// bootstrap the demo
var demo = new Vue({
  el: '#demo',
  data: {
    searchQuery: '',
    searchQueryOddsype: '',
    gridColumns: ['api_id', 'home_team', 'away_team', 'league', 'match_time', 'odds_type', 'update_time', 'home_odds', 'line', 'away_odds'],
    gridData: [],
    queryRangeDays: '1'
  },
  mounted () {
    this.btn_get_json_esOdds()
  },
  methods: {
    btn_get_json_esOdds: function () {
      let urlGetMatches = myconst.apiHostPort + '/get_json_esOdds'
      let url = urlGetMatches + '?day=' + this.queryRangeDays
      axios.get(url).then((response) => {
        this.gridData = response.data['data']
      })
    }
  }
})
